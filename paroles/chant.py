from pathlib import Path
import re


class Chant:

    def __init__(self, path: Path):
        with open(path, 'r') as file:
            text = file.read()
        try:
            paragraphs = self.extract_paragraphs(text)
            self.structure = self.extract_song_structure(paragraphs)
        except ValueError as err:
            raise ValueError(f'{err} in file {path}')

    @property
    def refrain(self):
        return self.structure['refrain']

    @property
    def couplets(self):
        return self.structure['couplets']

    def extract_paragraphs(self, song_text: str) -> list[str]:
        paragraphs = [paragraph.strip() for paragraph in song_text.split('\n\n')]
        return [p for p in paragraphs if p != '']

    def clean_paragraph(self, text: str) -> str:
        return re.sub('\n +', '\n', text).strip()

    def extract_song_structure(self, paragraphs: list[str]):
        REFRAIN_REGEX = r'(R|Refrain)[.\-):]?\s+((.|\n)*)'
        COUPLET_REGEX = r'(\d+)[.\-):]?\s+((.|\n)*)'
        structure = {
            'refrain': None,
            'couplets': []
        }
        for paragraph in paragraphs:
            refrain_match = re.match(REFRAIN_REGEX, paragraph)
            couplet_match = re.match(COUPLET_REGEX, paragraph)
            if refrain_match:
                if structure['refrain'] is None:
                    refrain = self.clean_paragraph(refrain_match.group(2))
                    structure['refrain'] = refrain
                else:
                    raise ValueError('Une chanson ne peut pas avoir plusieurs refrains.')
            elif couplet_match:
                index_str = couplet_match.group(1)
                if index_str is not None and int(index_str)-1 == len(structure['couplets']):
                    couplet = self.clean_paragraph(couplet_match.group(2))
                    structure['couplets'].append(couplet)
                else:
                    raise ValueError('La numérotation des couplets est invalide.')
            else:
                raise ValueError('Paragraphe invalide.')
        return structure



