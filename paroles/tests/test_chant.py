import pytest

from paroles.chant import Chant

def test_extract_paragraphs():
    chant = Chant('')
    text = '''
    first part

    second part
    still second part

    third part





    last part
    this is the end
    '''
    result = chant.extract_paragraphs(text)
    assert len(result) == 4

def test_extract_song_structure():
    chant = Chant('')
    text = '''
    R. refrain

    1. le premier couplet

    2. le second couplet
    '''
    paragraphs = chant.extract_paragraphs(text)
    structure = chant.extract_song_structure(paragraphs)
    assert structure['refrain'] == 'refrain'
    assert len(structure['couplets']) == 2
    assert structure['couplets'][0] == 'le premier couplet'
    assert structure['couplets'][1] == 'le second couplet'

def test_extract_song_structure_alt():
    chant = Chant('')
    text = '''
    Refrain. ceci est un refrain
    avec un nom

    1 Le premier couplet
    a un numéro

    2)  Le second couplet
    un autre
    '''
    paragraphs = chant.extract_paragraphs(text)
    structure = chant.extract_song_structure(paragraphs)
    assert structure['refrain'] == 'ceci est un refrain\navec un nom'
    assert len(structure['couplets']) == 2
    assert structure['couplets'][0] == 'Le premier couplet\na un numéro'
    assert structure['couplets'][1] == 'Le second couplet\nun autre'

def test_extract_song_structure_no_tag_error():
    chant = Chant('')
    text = '''
    sans nom
    '''
    paragraphs = chant.extract_paragraphs(text)
    with pytest.raises(ValueError):
        structure = chant.extract_song_structure(paragraphs)

def test_extract_song_structure_double_refrain_error():
    chant = Chant('')
    text = '''
    R. un refrain

    R. un autre refrain
    '''
    paragraphs = chant.extract_paragraphs(text)
    with pytest.raises(ValueError):
        structure = chant.extract_song_structure(paragraphs)
