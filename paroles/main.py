from chant import Chant
from slides import SlidesBuilder


if __name__ == '__main__':
    NAMES = [
        'db_chants/alleluia_jubilate.txt',
        'db_chants/venez_approchons_nous.txt',
        'db_chants/voici_le_corps_et_le_sang_du_seigneur.txt',
    ]
    chants = [Chant(name) for name in NAMES]
    slides_builder = SlidesBuilder(chants)

