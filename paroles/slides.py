from chant import Chant


PAGE_TEMPLATE = 'template.html'
BODY_TAG = '// BODY'
GENERATED_FILE = 'generated.html'

MAX_LINES_ON_SCREEN = 9


class HTMLContent:

    def __init__(self, content: str):
        self._content = content

    @property
    def source(self):
        return self._content

    def wrap_in_tag(self, tag_name: str,
                    **attributes: dict[str, str]):
        html_attributes = []
        for attr, value in attributes.items():
            if attr == '_class':
                # this hack is necessary because 'class' is a reserved keyword
                # in python
                attr = 'class'
            html_attributes.append(f'{attr}="{value}"')
        attr_line  = ' '.join(html_attributes)
        self._content = f'<{tag_name} {attr_line}>\n{self._content}\n</{tag_name}>'


class SlidesBuilder:

    def __init__(self, chants: list[Chant]):
        self.slides = []
        # build the slides from the different songs
        self.reset_buffer()
        for chant in chants:
            refrain_size = chant.refrain.count('\n')
            self.push_to_buffer(chant.refrain, refrain_size)
            for couplet in chant.couplets:
                couplet_size = couplet.count('\n')
                if self.can_fit_in_buffer(couplet_size):
                    self.push_to_buffer(couplet, couplet_size)
                else:
                    self.generate_next_slide()
                    self.push_to_buffer(chant.refrain, refrain_size)
                    self.push_to_buffer(couplet, couplet_size)
            self.generate_next_slide()
        html_content = self.join_elements(self.slides)
        html_content.wrap_in_tag('div', id='slides')
        # start link
        start = HTMLContent('_')
        start.wrap_in_tag('a', href=self.make_slide_tag(1, ref=True))
        start.wrap_in_tag('div', _class='start')
        final_content = self.join_elements([start, html_content])
        # insert in the page template
        with open(PAGE_TEMPLATE, 'r') as file:
            template = file.read()
        generated = template.replace(BODY_TAG, final_content.source)
        with open(GENERATED_FILE, 'w') as file:
            file.write(generated)

    def reset_buffer(self):
        self.lines_in_buffer = 0
        self.buffer = []

    def can_fit_in_buffer(self, size: int):
        return self.lines_in_buffer + size < MAX_LINES_ON_SCREEN

    def push_to_buffer(self, paragraph: str, size: int):
        self.buffer.append(paragraph)
        self.lines_in_buffer += size + 1 # We count one empty line between 
                                         # two paragraphs

    def generate_next_slide(self):
        slide = self.make_slide(self.buffer, len(self.slides) + 1)
        self.slides.append(slide)
        self.reset_buffer()

    def join_elements(self, elements: list[HTMLContent]) -> HTMLContent:
        elements_html = [elem.source for elem in elements]
        new_source = '\n'.join(elements_html)
        return HTMLContent(new_source)

    def make_slide_tag(self, slide_id: int, ref=False) -> str:
        slide_id = max(1, slide_id)
        if ref:
            return f'#slide-{slide_id}'
        else:
            return f'slide-{slide_id}'

    def make_slide(self, content: list[str], slide_id: int) -> HTMLContent:
        # slide ids
        previous_id = self.make_slide_tag(slide_id-1, ref=True)
        current_id = self.make_slide_tag(slide_id)
        next_id = self.make_slide_tag(slide_id+1, ref=True)
        # content of the slide
        html_paragraphs = []
        for paragraph in content:
            html_par = HTMLContent(paragraph.replace('\n', '<br>'))
            html_par.wrap_in_tag('p')
            html_paragraphs.append(html_par)
        content = self.join_elements(html_paragraphs)
        content.wrap_in_tag('div')
        content.wrap_in_tag('a', href=next_id)
        # nextarrow (go to next slide)
        nextarrow = HTMLContent('>')
        nextarrow.wrap_in_tag('a', href=next_id)
        nextarrow.wrap_in_tag('div', _class='nextarrow')
        # backarrow (go to previous slide)
        backarrow = HTMLContent('<')
        backarrow.wrap_in_tag('a', href=previous_id)
        backarrow.wrap_in_tag('div', _class='backarrow')
        section_content = self.join_elements([content, backarrow, nextarrow])
        section_content.wrap_in_tag('section', id=current_id)
        return section_content

