from setuptools import find_packages, setup

setup(
    name='paroles',
    version='0.0.1',
    description='Generate html slides with the lyrics',
    author='Baptiste Lambert',
    packages=find_packages(),
)
