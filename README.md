# Paroles

Un système d’affichage des paroles des chants.

# Utilisation

Lancer le programme :
```
python paroles/main.py
```

Puis ouvrir la page html générée : `paroles/generated.html`


Les chants sont enregistrés dans `paroles/db_chants` et leur sélection est configurée dans le script principal `paroles/main.py`.


